# Introduction
This document explains steps on configuring a master node and a worker node k8s cluster on Oracle Linux v7.9 (UEK5.x)
Well I did attempt to install it on Oracle Linux v8.x. It seems that the repositories has changed, and many others can't found especially the v8.x RedHat replaced docker with podman.
#### Single master node installation
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/ 
#### Multi master nodes installation - HA
- Deprecated, and hence didn't works as of 20210807
- https://docs.oracle.com/en/operating-systems/oracle-linux/kubernetes/kube_ha.html

### Enable all the repos - Oracle Linux v7.9
``` bash
vi /etc/yum.repos.d/oracle-linux*
each repo change 
enabled=0 --> enabled=1
```
### Enable all the repos - RedHat distro
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

### Configure iptables to see bridged traffic
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system


### Install necessary packages - RedHat Distro
```
yum install -y kubeadm kubelet kubectl kubeadm-ha-setup
```

### Install necessary packages - Debian Distro
```
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```
### Enable and start docker service
```
systemctl enable docker.service --now
```
### Enable firewall
```
firewall-cmd --permanent --add-masquerade
firewall-cmd --permanent --add-port={6443/tcp,8472/tcp,8472/udp,8080/tcp,53/tcp,53/udp,443/tcp,6379/tcp}
firewall-cmd --permanent --add-port={2379-2380/tcp,10250-10255/tcp}
firewall-cmd --reload
iptables -P FORWARD ACCEPT
firewall-cmd --direct --add-rule ipv4 filter INPUT 0 --in-interface ens33 --destination 224.0.0.18 --protocol vrrp -j ACCEPT
firewall-cmd --direct --add-rule ipv4 filter OUTPUT 0 --out-interface ens33 --destination 224.0.0.18 --protocol vrrp -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 0 --in-interface ens33 --destination 224.0.0.18 --protocol vrrp -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 0 --out-interface ens33 --destination 224.0.0.18 --protocol vrrp -j ACCEPT

```

### Set selinux to permissive and reboot
```
sed -i 's/SELINUX=enforcing/SELINUX=permissive/' /etc/selinux/config
reboot
```
### Copy template of configuration - If setting up multi-master-nodes-HA
- the template is provided by the installation
- Refer to (ha.yaml) 
```
cp /usr/local/share/kubeadm/kubeadm-ha/ha.yaml ~/ha.yaml    
view ha.yaml
```

### Synchronize time with NTP
```
yum -y install ntp
systemctl enable ntpd --now
```
### Run the setup script 
- Setup first master node
- The --token, --pod-network-cidr, --service-cidr are optional flags. If not specified default to auto-generated token, pod cidr to 10.244.0.0/16, and service cidr point to 10.96.0.0/12
```
kubeadm-setup.sh up \
	--token ymhydi.5jlmdadeb1ju1pgd \
	--pod-network-cidr 10.200.0.0/16 --service-cidr 10.100.0.0/16
```
- Once master01 is setup completed, it will display steps on log further log into cluster and join from another node
- to add other node into the cluster
```
kubeadm-setup.sh join 192.168.32.91:6443 \
--token ymhydi.5jlmdadeb1ju1pgd \
--discovery-token-ca-cert-hash sha256:1004f2f72e3ea7c8df3528af7ebd9fdb154b975ec470ed4eec20a37ce456bd89
```

### Untaint the master node so application can be scheduled on it
```
kubectl describe node master01.example.com | grep -i taint
kubectl taint node master01.example.com node-role.kubernetes.io/master:NoSchedule-
```

### Configure cgroup driver
Create the yaml definition
``` 
vi kubeadm-config.yaml
kind: ClusterConfiguration
apiVersion: kubeadm.k8s.io/v1beta3
kubernetesVersion: v1.21.0
---
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
cgroupDriver: systemd
```
Pass the configuration to kubeadm
```
kubeadm init --config kubeadm-config.yaml
```

### Grant cluster admin login
- Execute this cmd from non-root account
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
- Execute this from root, and grant non-root user cluster admin access
```
export KUBECONFIG=/etc/kubernetes/admin.conf
su - student
kubectl get nodes
kubectl whoami
```
- From worker node, copy this file
```
mkdir ~/.kube
scp master01:/etc/kubernetes/admin.conf ~/.kube/config
```

